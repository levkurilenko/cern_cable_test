// Engineer: Lev Kurilenko
// Date: 8/7/2017
// Module: Aurora Rx Top
// 
// Changelog: 
// (Lev Kurilenko) 8/7/2017 - Created top module

module aurora_rx_top(
    input sysclk_in_p,
    input sysclk_in_n,
    input rst_in,
    
    input data_in_p,
    input data_in_n,
    
    // output LCD_DB4_LS,
    // output LCD_DB5_LS,
    // output LCD_DB6_LS,
    // output LCD_DB7_LS,
    // output LCD_E_LS,
    // output LCD_RS_LS,
    // output LCD_RW_LS,
    
    output USER_SMA_CLOCK_P,
    output USER_SMA_CLOCK_N
);

// Resets
wire rst;

// Clocks
wire clk40;
// wire clk50; // For LCD
wire clk160;
wire clk640;
wire clk200;
wire clk300;
wire clk400;
wire mmcm_locked;

// ISERDES Signals
reg  [31:0] data32_iserdes;
wire [7:0]  sipo;

// Rx Gearbox Signals
wire        gearbox_rdy_rx;
wire [65:0] data66_gb_rx;
wire        data_valid;

// Descrambler Signals
wire [1:0]  sync_out;
wire [63:0] data_out;

// Block Sync Signals
wire        blocksync_out;
wire        rxgearboxslip_out;

// Bit Error Rate Signals
wire [63:0] ber_cnt;

// Bit Error Rate Logic
// reg [15:0] bit_err_cnt;
// reg [15:0] bit_err_cnt_next;
// reg [15:0] inv_data_cnt;
// reg [63:0] data64_latched;
// wire [63:0] data64_added;
// reg latched_true;

// Bitslip FSM Signals
wire        iserdes_slip;
wire        gearbox_slip;

// VIO Signals
wire        vio_rst;

assign rst = !mmcm_locked;

// Data Reception (8 bits to 32 bits)
always @(posedge clk160) begin
    if (rst) begin
        data32_iserdes <= 32'h0000_0000;
    end
    else begin
        data32_iserdes[31:24] <= sipo;
        data32_iserdes[23:16] <= data32_iserdes[31:24];
        data32_iserdes[15:8]  <= data32_iserdes[23:16];
        data32_iserdes[7:0]   <= data32_iserdes[15:8];
    end
end

reg [31:0] data32_iserdes_r;
reg [31:0] data32_iserdes_r_r;

always @(posedge clk40) begin
    if (rst) begin
        data32_iserdes_r <= 32'h0000_0000;
        data32_iserdes_r_r <= 32'h0000_0000;
    end
    else begin
        data32_iserdes_r <= data32_iserdes;
        data32_iserdes_r_r <= data32_iserdes_r;
    end
end

//============================================================================
//                      Iterating Through Tap Values
//============================================================================

reg [4:0] tap_value;    // 32 possible tap values
reg [28:0] delay_cnt;   // Large enough to count to 10 second given a 40 MHz clock
wire delay_locked;
wire [4:0] tap_out;
reg delay_ce;
reg delay_inc;
reg in_delay_reset;
reg [31:0] tap_sync_array;
reg blocksync_out_old;
reg [1:0] desync_cnt;
reg sync_once;

// Vio Signals
wire [4:0] vio_tap_value; // 32 possible tap values specified through the VIO
wire vio_tap_en;          // Enable VIO tap value override
wire vio_tap_set;         // This signal sets the VIO tap value. Must be set back to 0 before it can be used again
reg  tap_rst;             // This signal is used to make sure vio_tap_set is set back to 0 before a new tap value can be set

always @(posedge clk40) begin
    if (rst|vio_rst) begin
        tap_value <= 5'h00;
        delay_cnt <= 29'h0000_0000;
        delay_ce <= 1'b0;
        delay_inc <= 1'b0;
        in_delay_reset <= 1'b0;
        tap_sync_array <= 32'b0000_0000;
        blocksync_out_old <= 1'b0;
        desync_cnt <= 4'h0;
        tap_rst <= 1'b0;
        sync_once <= 1'b0;
    end
    else begin
        blocksync_out_old <= blocksync_out;
        
        if (!vio_tap_en) begin
            if ((blocksync_out_old == 0) && (blocksync_out == 1)) begin
                sync_once <= 1'b1;
            end
            if ((blocksync_out_old == 1) && (blocksync_out == 0) && (desync_cnt < 4)) begin
                desync_cnt <= desync_cnt + 1;
            end
            // Conditional checks if 1s has passed and if 3 desynchronization events have occured
            // or synchronization never occured    
            if ((delay_cnt >= 40_000_000) && ((desync_cnt >= 3)||(sync_once == 0))) begin // 1s delay
                if (!blocksync_out) begin
                    delay_ce <= 1'b1;
                    in_delay_reset <= 1'b1;
                    tap_value <= tap_value + 1;
                    desync_cnt <= 4'h0;
                end
                sync_once <= 1'b0;
                delay_cnt <= 29'h0000_0000;
            end
            else begin
                if (blocksync_out) begin
                        tap_sync_array[tap_value] <= 1'b1;
                end
                delay_cnt <= delay_cnt + 1;
                delay_ce <= 1'b0;
                in_delay_reset <= 1'b0;
            end
        end
        else if (vio_tap_en) begin
            // tap_rst used as a flag to indicate whether the tap needs to be reset or not
            // In other words, if tap_rst is high, the vio tap mechanism needs to be reset
            // by setting vio_tap_set back to 0. If tap_rst is low, a value of 1 on
            // vio_tap_set will trigger the tap setting mechanism 
            desync_cnt <= 4'h0;
            sync_once <= 1'b0;            
            if ((vio_tap_set)&&(tap_rst==0)) begin
                tap_rst <= 1'b1;
                delay_ce <= 1'b1;
                in_delay_reset <= 1'b1;
                tap_value <= vio_tap_value;
            end
            else begin
                if (blocksync_out) begin
                    tap_sync_array[tap_value] <= 1'b1;
                end
                if (!vio_tap_set) begin
                    tap_rst <= 1'b0;
                end
                delay_ce <= 1'b0;
                in_delay_reset <= 1'b0;
            end
        end
    end
end

//==========================
//  Clock Generation MMCM
//==========================
// Frequencies
// clk40:  40  MHz
// clk160: 160 MHz
// clk640: 640 MHz
// 
// clk_wiz_0 pll_fast(
//    .clk_in1_p(sysclk_in_p),
//    .clk_in1_n(sysclk_in_n),
//    .clk_out1(clk640),
//    .clk_out2(clk160),
//    .clk_out3(clk40),
//    .reset(rst_in),
//    .locked(mmcm_locked)
// );

// // Frequencies
// // clk40:  10  MHz
// // clk160: 40 MHz
// // clk640: 160 MHz
// // 
// // If this PLL is instantiated the clocks
// // will run at slower frequencies, despite
// // having names such as clk40, clk160, clk640.
// clk_wiz_1 pll_slow(
//    .clk_in1_p(sysclk_in_p),
//    .clk_in1_n(sysclk_in_n),
//    .clk_out1(clk640),
//    .clk_out2(clk160),
//    .clk_out3(clk40),
//    .clk_out4(clk200),
//    .clk_out5(clk50),
//    .reset(rst_in),
//    .locked(mmcm_locked)
// );
// 
// OBUFDS #(
//     .IOSTANDARD("DEFAULT"), // Specify the output I/O standard
//     .SLEW("FAST")           // Specify the output slew rate (Changed from "SLOW" [default])
// ) clk160_obufds (
//     .O(USER_SMA_CLOCK_P),   // Diff_p output (connect directly to top-level port)
//     .OB(USER_SMA_CLOCK_N),  // Diff_n output (connect directly to top-level port)
//     .I(clk200)              // Buffer input
// );

// Frequencies
// clk40:  10  MHz
// clk160: 40 MHz
// clk640: 160 MHz
// clk200: 200 MHz
// clk50:  50 MHZ
// Internal clocks generated from incoming clk sent over SMA
// clk_wiz_2 pll_inc_clk(
//    .clk_in1_p(USER_SMA_CLOCK_P),
//    .clk_in1_n(USER_SMA_CLOCK_N),
//    .clk_out1(clk640),
//    .clk_out2(clk160),
//    .clk_out3(clk40),
//    .reset(rst_in),
//    .locked(mmcm_locked)
// );

// Frequencies
// clk40:  39.0625  MHz
// clk160: 156.25 MHz
// clk640: 625 MHz
// clk200: 312.5 MHz
// clk50:  50 MHZ
// 
// If this PLL is instantiated the clocks
// will run at slower frequencies, despite
// having names such as clk40, clk160, clk640.
clk_wiz_3 pll_fast(
   .clk_in1_p(sysclk_in_p),
   .clk_in1_n(sysclk_in_n),
   .clk_out1(clk640),
   .clk_out2(clk160),
   .clk_out3(clk40),
   //.clk_out4(clk200),
   //.clk_out4(clk300),
   .clk_out4(clk400),
   //.clk_out5(clk50),
   .reset(rst_in),
   .locked(mmcm_locked)
);

OBUFDS #(
    .IOSTANDARD("DEFAULT"), // Specify the output I/O standard
    .SLEW("FAST")           // Specify the output slew rate (Changed from "SLOW" [default])
) clk160_obufds (
    .O(USER_SMA_CLOCK_P),   // Diff_p output (connect directly to top-level port)
    .OB(USER_SMA_CLOCK_N),  // Diff_n output (connect directly to top-level port)
    .I(clk160)              // Buffer input
);

// // Frequencies
// // clk40:  20 MHz
// // clk160: 80 MHz
// // clk640: 320 MHz
// // clk200: 200 MHz
// // clk50:  50 MHZ
// // 
// // If this PLL is instantiated the clocks
// // will run at slower frequencies, despite
// // having names such as clk40, clk160, clk640.
// clk_wiz_4 pll_mid(
//    .clk_in1_p(sysclk_in_p),
//    .clk_in1_n(sysclk_in_n),
//    .clk_out1(clk640),
//    .clk_out2(clk160),
//    .clk_out3(clk40),
//    .clk_out4(clk200),
//    .clk_out5(clk50),
//    .reset(rst_in),
//    .locked(mmcm_locked)
// );
// 
// OBUFDS #(
//     .IOSTANDARD("DEFAULT"), // Specify the output I/O standard
//     .SLEW("FAST")           // Specify the output slew rate (Changed from "SLOW" [default])
// ) clk160_obufds (
//     .O(USER_SMA_CLOCK_P),   // Diff_p output (connect directly to top-level port)
//     .OB(USER_SMA_CLOCK_N),  // Diff_n output (connect directly to top-level port)
//     .I(clk200)              // Buffer input
// );

//===================
// Aurora Rx
//===================
// // ISERDES without IDELAYCTRL or IDELAYE2
// cmd_iserdes i0 (
//     .data_in_from_pins_p(data_in_p),
//     .data_in_from_pins_n(data_in_n),
//     .clk_in(clk640),
//     .clk_div_in(clk160),
//     .io_reset(rst|vio_rst),
//     .bitslip(iserdes_slip),
//     .data_in_to_device(sipo)
// );

// wire delay_locked;
// // ISERDES with IDELAYCTRL or IDELAYE2
// selectio_wiz_0 i0 (
//   .data_in_from_pins_p(data_in_p),
//   .data_in_from_pins_n(data_in_n),
//   .clk_in(clk640),
//   .clk_div_in(clk160),
//   .io_reset(rst|vio_rst),
//   .in_delay_reset(rst|vio_rst),
//   .in_delay_data_ce(1'b0),
//   .in_delay_data_inc(1'b0),
//   .ref_clock(clk200),
//   .delay_locked(delay_locked),
//   .bitslip(iserdes_slip),
//   .data_in_to_device(sipo)
// );

// ISERDES with Fixed Tap Value
// selectio_wiz_0 i0 (
//   .data_in_from_pins_p(data_in_p),
//   .data_in_from_pins_n(data_in_n),
//   .clk_in(clk640),
//   .clk_div_in(clk160),
//   .io_reset(rst|vio_rst),
//   .ref_clock(clk200),
//   .delay_locked(delay_locked),
//   .bitslip(iserdes_slip),
//   .data_in_to_device(sipo)
// );

selectio_wiz_0 i0 (
  .data_in_from_pins_p(data_in_p),
  .data_in_from_pins_n(data_in_n),
  .clk_in(clk640),
  .clk_div_in(clk160),
  .io_reset(rst|vio_rst),

  .in_delay_reset(in_delay_reset),      // input wire in_delay_reset
  .in_delay_tap_in(tap_value),          // input wire [4 : 0] in_delay_tap_in
  .in_delay_tap_out(tap_out),           // output wire [4 : 0] in_delay_tap_out
  .in_delay_data_ce(delay_ce),          // input wire [0 : 0] in_delay_data_ce
  .in_delay_data_inc(delay_inc),        // input wire [0 : 0] in_delay_data_inc
  
  //.ref_clock(clk200),
  //.ref_clock(clk300),
  .ref_clock(clk400),
  .delay_locked(delay_locked),
  .bitslip(iserdes_slip),
  .data_in_to_device(sipo)
);

gearbox32to66 rx_gb (
    .rst(rst|vio_rst),
    .clk(clk40),
    .data32(data32_iserdes_r_r),
    .gearbox_rdy(gearbox_rdy_rx),
    .gearbox_slip(gearbox_slip),
    .data66(data66_gb_rx),
    .data_valid(data_valid)
);

descrambler uns (
    .clk(clk40),
    .rst(!blocksync_out|rst|vio_rst),
    .data_in(data66_gb_rx), 
    .sync_info(sync_out),
    .enable(blocksync_out&data_valid&gearbox_rdy_rx),
    .data_out(data_out)
);

block_sync # (
    .SH_CNT_MAX(16'd400),           // default: 64
    .SH_INVALID_CNT_MAX(10'd16)     // default: 16
)
b_sync (
    .clk(clk40),
    .system_reset(rst|vio_rst),
    .blocksync_out(blocksync_out),
    .rxgearboxslip_out(rxgearboxslip_out),
    .rxheader_in(sync_out),
    .rxheadervalid_in(data_valid&gearbox_rdy_rx)
);

bitslip_fsm bs_fsm (
    .clk(clk160),
    .rst(rst|vio_rst),
    .blocksync(blocksync_out),
    .rxgearboxslip(rxgearboxslip_out),
    .iserdes_slip(iserdes_slip),
    .gearbox_slip(gearbox_slip)
);

ber ber_inst(
    .rst(rst|vio_rst),
    .clk40(clk40),
    .blocksync_out(blocksync_out),
    .data_valid(data_valid),
    .gearbox_rdy_rx(gearbox_rdy_rx),
    .data66_gb_rx(data66_gb_rx),
    .data64_rx_uns(data64_rx_uns),
    .ber_cnt(ber_cnt)
);

//============================================================================
//                           Bit Error Rate Logic
//============================================================================
// always @(posedge clk40) begin
//     if (rst|vio_rst) begin
//         bit_err_cnt <= 16'h0000;
//         inv_data_cnt <= 16'h0000;
//         latched_true <= 1'b0;
//         data64_latched <= 64'h0000_0000_0000_0000;
//     end
//     else if (latched_true == 0) begin
//         bit_err_cnt <= bit_err_cnt_next;
//         
//         if (data_out == data64_latched + 1) begin
//             latched_true <= 1'b1;
//         end
//         else if (blocksync_out&data_valid&gearbox_rdy_rx) begin
//             data64_latched <= data_out;
//         end
//     end
//     else if (latched_true == 1) begin
//         bit_err_cnt <= bit_err_cnt_next;
//         
//         //if ((blocksync_out&data_valid&gearbox_rdy_rx)&&(data_out == data64_latched)) begin
//         //    latched_true <= 1'b0;
//         //    bit_err_cnt <= 16'h0000;
//         //    inv_data_cnt <= 16'h0000;
//         //end
//         if ( (blocksync_out&data_valid&gearbox_rdy_rx)&& ( (data_out != data64_latched + 1) || (sync_out != (2'b10 || 2'b01)) ) ) begin
//             inv_data_cnt <= inv_data_cnt + 1;
//             //data64_latched <= data64_latched + 1;
//         end
//         
//         if (blocksync_out&data_valid&gearbox_rdy_rx) begin
//             data64_latched <= data64_latched + 1;
//         end
//     end
// end
// 
// assign data64_added = data64_latched + 1;
// always @(*) begin
//     bit_err_cnt_next = bit_err_cnt;
//     if (rst|vio_rst) begin
//         bit_err_cnt_next = 16'h0000;
//     end
//     else if (blocksync_out&latched_true&data_valid) begin
//         for (int j = 0; j < 64; j = j + 1) begin
//             //bit_err_cnt_next = bit_err_cnt + (data_out[j] != data64_added[j]);
//             bit_err_cnt_next = bit_err_cnt_next + (data_out[j] != data64_added[j]);
//         end
//     end
// end

//============================================================================
//                          Debugging & Monitoring
//============================================================================

// ILA
ila_1 ila_slim (
    .clk(clk160),
    .probe0(rst),
    .probe1(blocksync_out),
    .probe2(data_out),
    .probe3(delay_ce),
    .probe4(delay_inc),
    .probe5(tap_value),
    .probe6(tap_sync_array),
    .probe7(ber_cnt),
    .probe8(tap_rst)
);

//ila_0 ila (
//	.clk(clk160),                  // input wire clk

//    .probe0(rst),                 // input wire [0:0]  probe0  
//	.probe1(clk40),               // input wire [0:0]  probe1 
//	.probe2(clk160),              // input wire [0:0]  probe2 
//	.probe3(1'b0),                // input wire [0:0]  probe3 
//    .probe4(mmcm_locked),         // input wire [0:0]  probe4 
//	.probe5(gearbox_rdy_rx),      // input wire [0:0]  probe5 
//	.probe6(data_valid),          // input wire [0:0]  probe6 
//	.probe7(blocksync_out),       // input wire [0:0]  probe7 
//	.probe8(rxgearboxslip_out),   // input wire [0:0]  probe8 
//	.probe9(iserdes_slip),        // input wire [0:0]  probe9 
//	.probe10(gearbox_slip),       // input wire [0:0]  probe10 
//	.probe11(data32_iserdes),     // input wire [31:0] probe11 
//    .probe12(sipo),               // input wire [7:0]  probe12 
//	.probe13(data66_gb_rx),       // input wire [65:0] probe13 
//	.probe14(sync_out),           // input wire [1:0]  probe14 
//	.probe15(data_out),           // input wire [63:0] probe15
//	.probe16(bit_err_cnt),        // input wire [15:0] probe16 
//    .probe17(bit_err_cnt_next),   // input wire [15:0] probe17 
//    .probe18(inv_data_cnt),       // input wire [15:0] probe18 
//    .probe19(data64_latched),     // input wire [63:0] probe19 
//    .probe20(data64_added),       // input wire [63:0] probe20 
//    .probe21(latched_true)        // input wire [0:0]  probe21
//);

// VIO
vio_0 vio (
  .clk(clk160),                 // input wire clk
  .probe_out0(vio_rst),         // output wire [0 : 0] probe_out0
  .probe_out1(vio_tap_value),   // output wire [4 : 0] probe_out1
  .probe_out2(vio_tap_en),      // output wire [0 : 0] probe_out2
  .probe_out3(vio_tap_set)      // output wire [0 : 0] probe_out3
);

// wire SF_CE0;

// LCD
// lcd lcd_debug (
//     .clk(clk50),
//     .rst(rst),
//     .SF_D({LCD_DB7_LS, LCD_DB6_LS, LCD_DB5_LS, LCD_DB4_LS}),
//     .LCD_E(LCD_E_LS),
//     .LCD_RS(LCD_RS_LS),
//     .LCD_RW(LCD_RW_LS),
//     .SF_CE0(SF_CE0),
//     .inv_data_cnt(inv_data_cnt)
// );
endmodule
