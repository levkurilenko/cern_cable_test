proc start_step { step } {
  set stopFile ".stop.rst"
  if {[file isfile .stop.rst]} {
    puts ""
    puts "*** Halting run - EA reset detected ***"
    puts ""
    puts ""
    return -code error
  }
  set beginFile ".$step.begin.rst"
  set platform "$::tcl_platform(platform)"
  set user "$::tcl_platform(user)"
  set pid [pid]
  set host ""
  if { [string equal $platform unix] } {
    if { [info exist ::env(HOSTNAME)] } {
      set host $::env(HOSTNAME)
    }
  } else {
    if { [info exist ::env(COMPUTERNAME)] } {
      set host $::env(COMPUTERNAME)
    }
  }
  set ch [open $beginFile w]
  puts $ch "<?xml version=\"1.0\"?>"
  puts $ch "<ProcessHandle Version=\"1\" Minor=\"0\">"
  puts $ch "    <Process Command=\".planAhead.\" Owner=\"$user\" Host=\"$host\" Pid=\"$pid\">"
  puts $ch "    </Process>"
  puts $ch "</ProcessHandle>"
  close $ch
}

proc end_step { step } {
  set endFile ".$step.end.rst"
  set ch [open $endFile w]
  close $ch
}

proc step_failed { step } {
  set endFile ".$step.error.rst"
  set ch [open $endFile w]
  close $ch
}

set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000

start_step init_design
set rc [catch {
  create_msg_db init_design.pb
  set_param gui.test TreeTableDev
  set_param simulator.modelsimInstallPath C:/modeltech64_10.5c/win64
  debug::add_scope template.lib 1
  set_property design_mode GateLvl [current_fileset]
  set_property webtalk.parent_dir C:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.cache/wt [current_project]
  set_property parent.project_path C:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.xpr [current_project]
  set_property ip_repo_paths c:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.cache/ip [current_project]
  set_property ip_output_repo c:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.cache/ip [current_project]
  add_files -quiet C:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.runs/synth_1/aurora_tx_four_lane_top.dcp
  add_files -quiet C:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.runs/vio_0_synth_1/vio_0.dcp
  set_property netlist_only true [get_files C:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.runs/vio_0_synth_1/vio_0.dcp]
  add_files -quiet C:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.runs/cmd_oserdes_synth_1/cmd_oserdes.dcp
  set_property netlist_only true [get_files C:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.runs/cmd_oserdes_synth_1/cmd_oserdes.dcp]
  add_files -quiet C:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.runs/clk_wiz_2_synth_1/clk_wiz_2.dcp
  set_property netlist_only true [get_files C:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.runs/clk_wiz_2_synth_1/clk_wiz_2.dcp]
  add_files -quiet C:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.runs/ila_1_synth_1/ila_1.dcp
  set_property netlist_only true [get_files C:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.runs/ila_1_synth_1/ila_1.dcp]
  read_xdc -mode out_of_context -ref vio_0 c:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.srcs/sources_1/ip/vio_0/vio_0_ooc.xdc
  set_property processing_order EARLY [get_files c:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.srcs/sources_1/ip/vio_0/vio_0_ooc.xdc]
  read_xdc -ref vio_0 c:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.srcs/sources_1/ip/vio_0/vio_0.xdc
  set_property processing_order EARLY [get_files c:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.srcs/sources_1/ip/vio_0/vio_0.xdc]
  read_xdc -mode out_of_context -ref cmd_oserdes -cells inst c:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.srcs/sources_1/ip/cmd_oserdes/cmd_oserdes_ooc.xdc
  set_property processing_order EARLY [get_files c:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.srcs/sources_1/ip/cmd_oserdes/cmd_oserdes_ooc.xdc]
  read_xdc -ref cmd_oserdes -cells inst c:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.srcs/sources_1/ip/cmd_oserdes/cmd_oserdes.xdc
  set_property processing_order EARLY [get_files c:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.srcs/sources_1/ip/cmd_oserdes/cmd_oserdes.xdc]
  read_xdc -mode out_of_context -ref clk_wiz_2 -cells inst c:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.srcs/sources_1/ip/clk_wiz_2/clk_wiz_2_ooc.xdc
  set_property processing_order EARLY [get_files c:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.srcs/sources_1/ip/clk_wiz_2/clk_wiz_2_ooc.xdc]
  read_xdc -prop_thru_buffers -ref clk_wiz_2 -cells inst c:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.srcs/sources_1/ip/clk_wiz_2/clk_wiz_2_board.xdc
  set_property processing_order EARLY [get_files c:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.srcs/sources_1/ip/clk_wiz_2/clk_wiz_2_board.xdc]
  read_xdc -ref clk_wiz_2 -cells inst c:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.srcs/sources_1/ip/clk_wiz_2/clk_wiz_2.xdc
  set_property processing_order EARLY [get_files c:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.srcs/sources_1/ip/clk_wiz_2/clk_wiz_2.xdc]
  read_xdc -mode out_of_context -ref ila_1 c:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.srcs/sources_1/ip/ila_1/ila_1_ooc.xdc
  set_property processing_order EARLY [get_files c:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.srcs/sources_1/ip/ila_1/ila_1_ooc.xdc]
  read_xdc -ref ila_1 c:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.srcs/sources_1/ip/ila_1/ila_v5_0/constraints/ila.xdc
  set_property processing_order EARLY [get_files c:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.srcs/sources_1/ip/ila_1/ila_v5_0/constraints/ila.xdc]
  read_xdc C:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.srcs/constrs_1/imports/constraints/aurora_tx.xdc
  link_design -top aurora_tx_four_lane_top -part xc7k325tffg900-2
  close_msg_db -file init_design.pb
} RESULT]
if {$rc} {
  step_failed init_design
  return -code error $RESULT
} else {
  end_step init_design
}

start_step opt_design
set rc [catch {
  create_msg_db opt_design.pb
  catch {write_debug_probes -quiet -force debug_nets}
  opt_design 
  write_checkpoint -force aurora_tx_four_lane_top_opt.dcp
  catch {report_drc -file aurora_tx_four_lane_top_drc_opted.rpt}
  close_msg_db -file opt_design.pb
} RESULT]
if {$rc} {
  step_failed opt_design
  return -code error $RESULT
} else {
  end_step opt_design
}

start_step place_design
set rc [catch {
  create_msg_db place_design.pb
  place_design 
  write_checkpoint -force aurora_tx_four_lane_top_placed.dcp
  catch { report_io -file aurora_tx_four_lane_top_io_placed.rpt }
  catch { report_clock_utilization -file aurora_tx_four_lane_top_clock_utilization_placed.rpt }
  catch { report_utilization -file aurora_tx_four_lane_top_utilization_placed.rpt -pb aurora_tx_four_lane_top_utilization_placed.pb }
  catch { report_control_sets -verbose -file aurora_tx_four_lane_top_control_sets_placed.rpt }
  close_msg_db -file place_design.pb
} RESULT]
if {$rc} {
  step_failed place_design
  return -code error $RESULT
} else {
  end_step place_design
}

start_step route_design
set rc [catch {
  create_msg_db route_design.pb
  route_design 
  write_checkpoint -force aurora_tx_four_lane_top_routed.dcp
  catch { report_drc -file aurora_tx_four_lane_top_drc_routed.rpt -pb aurora_tx_four_lane_top_drc_routed.pb }
  catch { report_timing_summary -warn_on_violation -max_paths 10 -file aurora_tx_four_lane_top_timing_summary_routed.rpt -rpx aurora_tx_four_lane_top_timing_summary_routed.rpx }
  catch { report_power -file aurora_tx_four_lane_top_power_routed.rpt -pb aurora_tx_four_lane_top_power_summary_routed.pb }
  catch { report_route_status -file aurora_tx_four_lane_top_route_status.rpt -pb aurora_tx_four_lane_top_route_status.pb }
  close_msg_db -file route_design.pb
} RESULT]
if {$rc} {
  step_failed route_design
  return -code error $RESULT
} else {
  end_step route_design
}

start_step write_bitstream
set rc [catch {
  create_msg_db write_bitstream.pb
  write_bitstream -force aurora_tx_four_lane_top.bit 
  if { [file exists C:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.runs/synth_1/aurora_tx_four_lane_top.hwdef] } {
    catch { write_sysdef -hwdef C:/Users/PC/Desktop/CERN_Pixel/Firmware/aurora_four_lane_fmc/aurora_tx_four_lane_640/aurora_tx.runs/synth_1/aurora_tx_four_lane_top.hwdef -bitfile aurora_tx_four_lane_top.bit -meminfo aurora_tx_four_lane_top.mmi -file aurora_tx_four_lane_top.sysdef }
  }
  close_msg_db -file write_bitstream.pb
} RESULT]
if {$rc} {
  step_failed write_bitstream
  return -code error $RESULT
} else {
  end_step write_bitstream
}

