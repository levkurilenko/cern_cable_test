################################## Clock Constraints ##########################
#create_clock -period 6.400 -waveform {0.000 3.200} [get_ports USER_SMA_CLOCK_P]
#create_clock -period 5.000 -waveform {0.000 2.500} [get_ports USER_SMA_CLOCK_P]
#create_clock -period 3.200 -waveform {0.000 1.600} [get_ports USER_SMA_CLOCK_P]
#create_clock -period 6.25 -waveform {0.000 3.125} [get_ports USER_SMA_CLOCK_P]
create_clock -period 12.5 -waveform {0.000 6.25} [get_ports USER_SMA_CLOCK_P]
#create_clock -period 25.00000000000000000 -waveform {0.00000000000000000 12.50000000000000000} [get_ports USER_SMA_CLOCK_P]

#create_generated_clock -name ttc_decoder_i/rclk -source [get_pins pll_i/clk_out1] -divide_by 3 [get_pins ttc_decoder_i/sample_reg/Q]
#create_generated_clock -name phase_sel_i/clk40_i -source [get_pins ttc_decoder_i/sample_reg/Q] -divide_by 4 [get_pins phase_sel_i/clk_out_reg/Q]
#create_generated_clock -source [get_pins phase_sel_i/clk_out_reg/Q] -multiply_by 2 [get_pins cout_i/recovered_clk/inst/clk_out1]
#create_generated_clock -source [get_pins phase_sel_i/clk_out_reg/Q] -multiply_by 2 [get_pins cout_i/recovered_clk/clk_out1]

################################# Location constraints ########################

##### LOCATIONS ARE FOR XILINX KC705 BOARD ONLY

#Reset input - GPIO_SW_N
set_property PACKAGE_PIN AA12 [get_ports rst_in]
set_property IOSTANDARD LVCMOS15 [get_ports rst_in]

#Sys/Rst Clk - built into board 200MHz
# set_property PACKAGE_PIN AD11 [get_ports sysclk_in_n]
# set_property IOSTANDARD LVDS [get_ports sysclk_in_n]
# set_property PACKAGE_PIN AD12 [get_ports sysclk_in_p]
# set_property IOSTANDARD LVDS [get_ports sysclk_in_p]


################################# FMC OSERDES Output ########################
## Lane 0
##FMC_LPC_LA06_P
#set_property PACKAGE_PIN AK20 [get_ports data_out_p[0]]
#set_property IOSTANDARD LVDS_25 [get_ports data_out_p[0]]
##FMC_LPC_LA06_N
#set_property PACKAGE_PIN AK21 [get_ports data_out_n[0]]
#set_property IOSTANDARD LVDS_25 [get_ports data_out_n[0]]

### Lane 0
###FMC_LPC_LA02_P
##set_property PACKAGE_PIN AF20 [get_ports data_out_p[0]]
##set_property IOSTANDARD LVDS_25 [get_ports data_out_p[0]]
###FMC_LPC_LA02_N
##set_property PACKAGE_PIN AF21 [get_ports data_out_n[0]]
##set_property IOSTANDARD LVDS_25 [get_ports data_out_n[0]]

## Lane 1
##FMC_LPC_LA03_P
#set_property PACKAGE_PIN AG20 [get_ports data_out_p[1]]
#set_property IOSTANDARD LVDS_25 [get_ports data_out_p[1]]
##FMC_LPC_LA03_N
#set_property PACKAGE_PIN AH20 [get_ports data_out_n[1]]
#set_property IOSTANDARD LVDS_25 [get_ports data_out_n[1]]

## Lane 2
##FMC_LPC_LA04_N
#set_property PACKAGE_PIN AH21 [get_ports data_out_p[2]]
#set_property IOSTANDARD LVDS_25 [get_ports data_out_p[2]]
##FMC_LPC_LA04_N
#set_property PACKAGE_PIN AJ21 [get_ports data_out_n[2]]
#set_property IOSTANDARD LVDS_25 [get_ports data_out_n[2]]

## Lane 3
##FMC_LPC_LA05_P
#set_property PACKAGE_PIN AG22 [get_ports data_out_p[3]]
#set_property IOSTANDARD LVDS_25 [get_ports data_out_p[3]]
##FMC_LPC_LA05_N
#set_property PACKAGE_PIN AH22 [get_ports data_out_n[3]]
#set_property IOSTANDARD LVDS_25 [get_ports data_out_n[3]]


################################# FMC Clock Input ########################
##USER FMC CLOCK
#set_property PACKAGE_PIN AD23 [get_ports USER_SMA_CLOCK_P]
#set_property IOSTANDARD LVDS_25 [get_ports USER_SMA_CLOCK_P]
#set_property PACKAGE_PIN AE24 [get_ports USER_SMA_CLOCK_N]
#set_property IOSTANDARD LVDS_25 [get_ports USER_SMA_CLOCK_N]


################################# SMA OSERDES Output ########################
# OSERDES Output
# USER_GPIO_P
# set_property IOSTANDARD LVDS_25 [get_ports data_out_p]
# USER_GPIO_N
# set_property PACKAGE_PIN Y24 [get_ports data_out_n]
# set_property IOSTANDARD LVDS_25 [get_ports data_out_n]


################################# SMA Clock Input ########################
#USER SMA CLOCK
set_property PACKAGE_PIN L25 [get_ports USER_SMA_CLOCK_P]
set_property IOSTANDARD LVDS_25 [get_ports USER_SMA_CLOCK_P]
set_property DIFF_TERM TRUE [get_ports USER_SMA_CLOCK_P]
set_property PACKAGE_PIN K25 [get_ports USER_SMA_CLOCK_N]
set_property IOSTANDARD LVDS_25 [get_ports USER_SMA_CLOCK_N]    
set_property DIFF_TERM TRUE [get_ports USER_SMA_CLOCK_N]


######################## FMC HPC IO Buffer OSERDES Output ########################
## Lane 0
##FMC_LPC_LA06_P
#set_property PACKAGE_PIN AK20 [get_ports data_out_p[0]]
#set_property IOSTANDARD LVDS_25 [get_ports data_out_p[0]]
##FMC_LPC_LA06_N
#set_property PACKAGE_PIN AK21 [get_ports data_out_n[0]]
#set_property IOSTANDARD LVDS_25 [get_ports data_out_n[0]]

# Lane 0
#FMC_HPC_LA02_P
set_property PACKAGE_PIN H24 [get_ports data_out_p[0]]
set_property IOSTANDARD LVDS_25 [get_ports data_out_p[0]]
#FMC_HPC_LA02_N
set_property PACKAGE_PIN H25 [get_ports data_out_n[0]]
set_property IOSTANDARD LVDS_25 [get_ports data_out_n[0]]

# Lane 1
#FMC_HPC_LA03_P
set_property PACKAGE_PIN H26 [get_ports data_out_p[1]]
set_property IOSTANDARD LVDS_25 [get_ports data_out_p[1]]
#FMC_HPC_LA03_N
set_property PACKAGE_PIN H27 [get_ports data_out_n[1]]
set_property IOSTANDARD LVDS_25 [get_ports data_out_n[1]]

# Lane 2
#FMC_HPC_LA04_N
set_property PACKAGE_PIN G28 [get_ports data_out_p[2]]
set_property IOSTANDARD LVDS_25 [get_ports data_out_p[2]]
#FMC_HPC_LA04_N
set_property PACKAGE_PIN F28 [get_ports data_out_n[2]]
set_property IOSTANDARD LVDS_25 [get_ports data_out_n[2]]

# Lane 3
#FMC_HPC_LA05_P
set_property PACKAGE_PIN G29 [get_ports data_out_p[3]]
set_property IOSTANDARD LVDS_25 [get_ports data_out_p[3]]
#FMC_HPC_LA05_N
set_property PACKAGE_PIN F30 [get_ports data_out_n[3]]
set_property IOSTANDARD LVDS_25 [get_ports data_out_n[3]]

######################### IO Buffer Driver Ports ########################
#FMC_LPC_LA05_P
set_property PACKAGE_PIN D21 [get_ports latch]
set_property IOSTANDARD LVCMOS25 [get_ports latch]

#FMC_LPC_LA05_P
set_property PACKAGE_PIN C21 [get_ports clk_io]
set_property IOSTANDARD LVCMOS25 [get_ports clk_io]

#FMC_LPC_LA05_P
set_property PACKAGE_PIN H21 [get_ports ser_in]
set_property IOSTANDARD LVCMOS25 [get_ports ser_in]

################################# LCD Ports ########################
#GPIO LCD
#set_property PACKAGE_PIN AA13 [get_ports LCD_DB4_LS]
#set_property IOSTANDARD LVCMOS15 [get_ports LCD_DB4_LS]
#set_property PACKAGE_PIN AA10 [get_ports LCD_DB5_LS]
#set_property IOSTANDARD LVCMOS15 [get_ports LCD_DB5_LS]
#set_property PACKAGE_PIN AA11 [get_ports LCD_DB6_LS]
#set_property IOSTANDARD LVCMOS15 [get_ports LCD_DB6_LS]
#set_property PACKAGE_PIN Y10 [get_ports LCD_DB7_LS]
#set_property IOSTANDARD LVCMOS15 [get_ports LCD_DB7_LS]
#set_property PACKAGE_PIN AB10 [get_ports LCD_E_LS]
#set_property IOSTANDARD LVCMOS15 [get_ports LCD_E_LS]
#set_property PACKAGE_PIN Y11 [get_ports LCD_RS_LS]
#set_property IOSTANDARD LVCMOS15 [get_ports LCD_RS_LS]
#set_property PACKAGE_PIN AB13 [get_ports LCD_RW_LS]
#set_property IOSTANDARD LVCMOS15 [get_ports LCD_RW_LS]

# Set False clock paths
#set_false_path -from [get_pins ttc_decoder_i/posOR_reg_reg/C] -to [get_pins ttc_decoder_i/sample_reg/D]
#set_false_path -from [get_clocks clk_out2_clk_wiz_0] -to [get_clocks ttc_decoder_i/rclk]
#set_false_path -from [get_clocks clk_out3_clk_wiz_0] -to [get_clocks clk_out1_clk_wiz_0]

set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clk160]
