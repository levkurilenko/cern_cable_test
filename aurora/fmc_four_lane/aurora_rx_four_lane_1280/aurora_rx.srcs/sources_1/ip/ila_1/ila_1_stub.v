// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4.1 (lin64) Build 2117270 Tue Jan 30 15:31:13 MST 2018
// Date        : Fri May 25 10:38:05 2018
// Host        : GSVHonestMistake running 64-bit Ubuntu 16.04.4 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/tony/src/UW/research/Hauck_LHC/cern_cable_test/aurora/fmc_four_lane/aurora_rx_four_lane_1280/aurora_rx.srcs/sources_1/ip/ila_1/ila_1_stub.v
// Design      : ila_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k325tffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "ila,Vivado 2017.4.1" *)
module ila_1(clk, probe0, probe1, probe2, probe3, probe4, probe5, 
  probe6, probe7, probe8)
/* synthesis syn_black_box black_box_pad_pin="clk,probe0[0:0],probe1[3:0],probe2[255:0],probe3[255:0],probe4[7:0],probe5[3:0],probe6[0:0],probe7[0:0],probe8[7:0]" */;
  input clk;
  input [0:0]probe0;
  input [3:0]probe1;
  input [255:0]probe2;
  input [255:0]probe3;
  input [7:0]probe4;
  input [3:0]probe5;
  input [0:0]probe6;
  input [0:0]probe7;
  input [7:0]probe8;
endmodule
