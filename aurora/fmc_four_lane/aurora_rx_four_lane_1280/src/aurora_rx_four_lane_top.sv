// Engineer: Lev Kurilenko
// Email: levkur@uw.edu
// Date: 11/28/2017
// Module: Aurora Rx Top w/ Support for multiple Aurora Lanes

module aurora_rx_four_lane_top (
    input sysclk_in_p,
    input sysclk_in_n,
    input rst_in,
    
    input [num_lanes-1:0] data_in_p,
    input [num_lanes-1:0] data_in_n,
    
    output USER_SMA_CLOCK_P,
    output USER_SMA_CLOCK_N
);
localparam num_lanes = 4;       // Specify number of desired lanes. Needs further modifications to make this general purpose.


// Resets
wire rst;

// Clocks
wire clk40;
wire clk160;
wire clk640;
wire clk400;
wire mmcm_locked, mmcm_locked_a; // For clk40, clk160, and clk400
wire pll_locked; // For clk640

// Aurora Rx Core Signals
wire [63:0] data_out[num_lanes];
wire [1:0]  sync_out[num_lanes];
wire [3:0]  blocksync_out;
wire [3:0]  gearbox_rdy_rx;
wire [3:0]  data_valid;

// Aurora Channel Bonding Signals
wire [63:0] data_out_cb[num_lanes];
wire [1:0]  sync_out_cb[num_lanes];
wire data_valid_cb;
wire channel_bonded;

// VIO Signals
wire        vio_rst;

assign mmcm_locked = mmcm_locked_a & pll_locked;
assign rst = !mmcm_locked;

//==========================
//  Clock Generation MMCM
//==========================

/**
* Depending on what bitrate the lanes are running at, different PLL and OBUFDS instantiations are needed.
* Uncomment pll_slow and clk160_obufds_slow blocks for a bitrate of 320 Mbps.
* Uncomment pll_fast and clk160_obufds_fast blocks for a bitrate of 1.28 Gbps.
**/

// Frequencies
// clk640: 640 MHz
// clk160: 160 MHz
// clk40:  40  MHz
// clk400: 400 MHz


clk_wiz_3 pll_med(
   .clk_in1_p(sysclk_in_p),
   .clk_in1_n(sysclk_in_n),
   .clk_out1(clk640),
   .clk_out2(clk160),
   .clk_out3(clk40),
   .reset(rst_in),
   .locked(mmcm_locked_a)
);
clk_wiz_6 pll_400_from_160(
    .clk_in1(clk160),
    .clk_out400(clk400),
    .reset(rst_in),
    .locked(pll_locked)
);


//// Frequencies
//// clk640: 160 MHz
//// clk160: 40 MHz
//// clk40:  10  MHz
//// clk400: 400 MHz
//// 
//// WARNING: If this PLL is instantiated the clocks
//// will run at slower frequencies, despite
//// having names such as clk640, clk160, clk40.
//clk_wiz_0 pll_mid(
//   .clk_in1_p(sysclk_in_p),
//   .clk_in1_n(sysclk_in_n),
//   .clk_out1(clk640),
//   .clk_out2(clk160),
//   .clk_out3(clk40),
//   .clk_out4(clk400),
//   .reset(rst_in),
//   .locked(mmcm_locked)
//);

//// Frequencies
//// clk640: 320 MHz
//// clk160: 80 MHz
//// clk40:  20  MHz
//// clk400: 400 MHz
////
//// WARNING: If this PLL is instantiated the clocks
//// will run at slower frequencies, despite
//// having names such as clk640, clk160, clk40.
////wire clk160_forward;
/*
clk_wiz_2 pll_mid_high(
    .clk_in1_p(sysclk_in_p),
    .clk_in1_n(sysclk_in_n),
   .clk_out1(clk640),
   .clk_out2(clk160),
   .clk_out3(clk40),
   .reset(rst_in),
   .locked(mmcm_locked_a)
);
clk_wiz_5 pll_400_from_80(
    .clk_in1(clk160),
    .clk_out400(clk400),
    .reset(rst_in),
    .locked(pll_locked)
);
*/

//// Frequencies
//// clk640: 80 MHz
//// clk160: 20 MHz
//// clk40:  5  MHz
//// clk400: 400 MHz
//// 
//// WARNING: If this PLL is instantiated the clocks
//// will run at slower frequencies, despite
//// having names such as clk640, clk160, clk40.
//wire clk160_forward;

//clk_wiz_1 pll_slow(
//   .clk_in1_p(sysclk_in_p),
//   .clk_in1_n(sysclk_in_n),
//   .clk_out1(clk640),
//   .clk_out2(clk160),
//   .clk_out3(clk40),
//   .clk_out4(clk160_forward),
//   .reset(rst_in),
//   .locked(mmcm_locked)
//);

// OBUFDS Slow
//OBUFDS #(
//    .IOSTANDARD("DEFAULT"), // Specify the output I/O standard
//    .SLEW("FAST")           // Specify the output slew rate (Changed from "SLOW" [default])
//) clk160_obufds_slow (
//    .O(USER_SMA_CLOCK_P),   // Diff_p output (connect directly to top-level port)
//    .OB(USER_SMA_CLOCK_N),  // Diff_n output (connect directly to top-level port)
//    .I(clk160_forward)              // Buffer input
//);

//// OBUFDS Mid
//OBUFDS #(
//    .IOSTANDARD("DEFAULT"), // Specify the output I/O standard
//    .SLEW("FAST")           // Specify the output slew rate (Changed from "SLOW" [default])
//) clk160_obufds_mid (
//    .O(USER_SMA_CLOCK_P),   // Diff_p output (connect directly to top-level port)
//    .OB(USER_SMA_CLOCK_N),  // Diff_n output (connect directly to top-level port)
//    .I(clk640)              // Buffer input
//);

// OBUFDS Fast
OBUFDS #(
    .IOSTANDARD("DEFAULT"), // Specify the output I/O standard
    .SLEW("FAST")           // Specify the output slew rate (Changed from "SLOW" [default])
) clk160_obufds_fast (
    .O(USER_SMA_CLOCK_P),   // Diff_p output (connect directly to top-level port)
    .OB(USER_SMA_CLOCK_N),  // Diff_n output (connect directly to top-level port)
    .I(clk160)              // Buffer input
);

//======================================
//              Aurora Rx
//======================================

/**
* The generate block will generate a variable amount of lanes based off the num_lanes variable.
* Uncomment aurora_rx_top      block in generate loop for bitrates less than 640 Mbps.
* Uncomment aurora_rx_top_xapp block in generate loop for bitrates more than 640 Mbps.
**/

wire ref_clk_bufg;
wire idelay_rdy;

BUFG
 ref_clock_bufg (
    .I (clk400),
    .O (ref_clk_bufg)
);

(* IODELAY_GROUP = "xapp_idelay" *)
IDELAYCTRL
 delayctrl (
    .RDY    (idelay_rdy),
    .REFCLK (ref_clk_bufg),
    .RST    (rst|vio_rst)
);

genvar i;

generate
    for (i=0; i < num_lanes; i=i+1)
        begin : rx_core
            //aurora_rx_top rx_lane (
            //    .rst(rst|vio_rst),
            //    .clk40(clk40),
            //    .clk160(clk160),
            //    .clk640(clk640),
            //    .data_in_p(data_in_p[i]),
            //    .data_in_n(data_in_n[i]),
            //    .blocksync_out(blocksync_out[i]),
            //    .gearbox_rdy(gearbox_rdy_rx[i]),
            //    .data_valid(data_valid[i]),
            //    .sync_out(sync_out[i]),
            //    .data_out(data_out[i])
            //);
            
            aurora_rx_top_xapp rx_lane (
                .rst(rst|vio_rst),
                .clk40(clk40),
                .clk160(clk160),
                .clk640(clk640),
                .clk400(clk400),
                .data_in_p(data_in_p[i]),
                .data_in_n(data_in_n[i]),
                .idelay_rdy(idelay_rdy),
                .blocksync_out(blocksync_out[i]),
                .gearbox_rdy(gearbox_rdy_rx[i]),
                .data_valid(data_valid[i]),
                .sync_out(sync_out[i]),
                .data_out(data_out[i])
            );
    end
endgenerate

//======================================
//      Aurora Channel Bonding
//======================================
channel_bond cb (
    .rst(rst|vio_rst),
    .clk40(clk40),
    .data_in(data_out),
    .sync_in(sync_out),
    .blocksync_out(blocksync_out),
    .gearbox_rdy_rx(gearbox_rdy_rx),
    .data_valid(data_valid),
    .data_out_cb(data_out_cb),
    .sync_out_cb(sync_out_cb),
    .data_valid_cb(data_valid_cb),
    .channel_bonded(channel_bonded)
);

//============================================================================
//                          Debugging & Monitoring
//============================================================================

// ILA
ila_1 ila_slim (
    .clk(clk160),
    .probe0(rst|vo_rst),       // output wire [0 : 0] probe_out0
    .probe1(blocksync_out),     // output wire [3 : 0] probe_out1
    .probe2({data_out[3], data_out[2], data_out[1], data_out[0]}),          // output wire [255 : 0] probe_out2
    .probe3({data_out_cb[3], data_out_cb[2], data_out_cb[1], data_out_cb[0]}),       // output wire [255 : 0] probe_out3
    .probe4({sync_out_cb[3], sync_out_cb[2], sync_out_cb[1], sync_out_cb[0]}),       // output wire [7 : 0] probe_out4
    .probe5(gearbox_rdy_rx),    // output wire [3 : 0] probe_out5
    .probe6(data_valid_cb),     // output wire [0 : 0] probe_out6
    .probe7(channel_bonded),    // output wire [0 : 0] probe_out7
    .probe8({sync_out[3], sync_out[2], sync_out[1], sync_out[0]})           // output wire [8 : 0] probe_out8
);

// VIO
vio_0 vio (
  .clk(clk160),                 // input wire clk
  .probe_out0(vio_rst)          // output wire [0 : 0] probe_out0
);

endmodule
