vlib work
vlib riviera

vlib riviera/xil_defaultlib
vlib riviera/xpm

vmap xil_defaultlib riviera/xil_defaultlib
vmap xpm riviera/xpm

vlog -work xil_defaultlib  -sv2k12 "+incdir+../../../ipstatic" "+incdir+/usr/local/xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../ipstatic" "+incdir+/usr/local/xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"/usr/local/xilinx/Vivado/2017.4/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/usr/local/xilinx/Vivado/2017.4/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -93 \
"/usr/local/xilinx/Vivado/2017.4/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../ipstatic" "+incdir+/usr/local/xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../ipstatic" "+incdir+/usr/local/xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../../aurora_rx.srcs/sources_1/ip/clk_wiz_6/clk_wiz_6_clk_wiz.v" \
"../../../../aurora_rx.srcs/sources_1/ip/clk_wiz_6/clk_wiz_6.v" \

vlog -work xil_defaultlib \
"glbl.v"

