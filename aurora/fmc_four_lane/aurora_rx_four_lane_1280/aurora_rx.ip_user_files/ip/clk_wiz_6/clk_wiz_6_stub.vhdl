-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4.1 (lin64) Build 2117270 Tue Jan 30 15:31:13 MST 2018
-- Date        : Fri May 25 14:25:56 2018
-- Host        : GSVHonestMistake running 64-bit Ubuntu 16.04.4 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/tony/src/UW/research/Hauck_LHC/cern_cable_test/aurora/fmc_four_lane/aurora_rx_four_lane_1280/aurora_rx.srcs/sources_1/ip/clk_wiz_6/clk_wiz_6_stub.vhdl
-- Design      : clk_wiz_6
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7k325tffg900-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity clk_wiz_6 is
  Port ( 
    clk_out400 : out STD_LOGIC;
    reset : in STD_LOGIC;
    locked : out STD_LOGIC;
    clk_in1 : in STD_LOGIC
  );

end clk_wiz_6;

architecture stub of clk_wiz_6 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk_out400,reset,locked,clk_in1";
begin
end;
